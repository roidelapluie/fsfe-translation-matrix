# Transation FSFE Matrix Generator


## How to clone the FSFE Web repo with git-svn

(because SVN is sooooo 1990...)

```
git svn clone https://svn.fsfe.org/fsfe-web/ -s
```

That will fail around buggy SVN revision 20902. Workaround is to run after the
failed clone:

```
cd fsfe-web
git svn fetch -r 20922:HEAD
```

The repo should be cloned now.

## Running the script

Copy analyze.sh and transation-status.sh to the repo directory.

Run:

```
./transation-status.sh > out.html
```

The result will be in out.html.

## License

- Scripts licensed under the GPLv3 or later.
- Contains the [sorttable.js
  library](http://www.kryogenix.org/code/browser/sorttable/), licensed under X11 license

