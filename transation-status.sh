#!/bin/bash

getlanguages(){
    for i in index.*.xhtml;
    do
        echo $i|cut -d . -f 2
    done|grep -xv en
}
echo "<html>"
echo "<head>"
echo "<script src=\"sorttable.js\"></script>"
echo "<style>"
echo "h1, li, p, td, th {font-family: monospace;}"
echo "td, th {text-align:center; padding: .3em}"
echo "</style>"
echo "</head>"
echo "<body>"
echo "<h1>Translations</h1>"
echo "<p>If you enable JavaScript you can sort the table by clicking on the first row.</p>"
echo "<h2>Legend</h2>"
echo "<ul><li>-2: No translation</li><li>-1: Translation is newer than EN version</li><li>X: Translation is X days older than EN version</li></ul>"
echo "<table class=\"sortable\">"
echo "<tr>"
for i in "Filename" $(getlanguages)
do
echo "<th>"
echo "$i"
echo "</th>"
done
echo "</tr>"
find . -type f -name '*.en.xhtml' -exec ./analyze.sh '{}' $(getlanguages) ';' | sort
echo "</table>"
echo "</body>"
echo "</html>"
