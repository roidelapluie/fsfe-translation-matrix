#!/bin/bash

filename="$1"
orig_change=$(git log -1 --format="%at" "$filename")

echo -n "<tr>"
echo -n "<td>$filename</td>"

shift
for lang in "$@"
do
    newfile="$(echo "$filename"|sed "s/.en.xhtml$/.${lang}.xhtml/")"
echo -n "<td style=\"background-color:"

if test -f $newfile
then
    change="$(git log -1 --format="%at" "$newfile")" 
    if [[ "$orig_change" -gt "$change" ]]
    then
        echo -n yellow
        diff=$((($orig_change-$change)/86400))
        if [[ $diff -gt 999 ]]
        then
        echo -n "\">999</td>"
    else
        echo -n "\">$diff</td>"
    fi
    else
        echo -n green
echo -n "\">-1</td>"
    fi
else
    echo -n red
echo -n "\">-2</td>"
fi



done


echo "</tr>"
